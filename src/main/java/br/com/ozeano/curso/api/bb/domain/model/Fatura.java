package br.com.ozeano.curso.api.bb.domain.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class Fatura {

	private Long id;
	private BigDecimal valor;
	private LocalDate dataVencimento;
	private TipoFatura tipo;
	private TipoPagamento tipoPagamento;
	private SituacaoFatura situacao;
	private String numeroDocumento;
	private String nossoNumero;
	private LocalDateTime criadoEm;
	private LocalDateTime atualizadoEm;
	
}
